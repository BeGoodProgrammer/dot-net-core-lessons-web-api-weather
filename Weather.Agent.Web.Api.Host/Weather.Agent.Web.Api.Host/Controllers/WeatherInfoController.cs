﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Weather.Agent.Web.Api.Host.Models;

namespace Weather.Agent.Web.Api.Host.Controllers
{
    [Route("api/[controller]")]
    public class WeatherInfoController : Controller
    {
        private static List<WeatherInfo> weatherInfoList =
            new List<WeatherInfo>
                {
                    new WeatherInfo{
                        Id = 1,
                        City = "Minsk",
                        Date = DateTime.Today,
                        MinTemerature = 25,
                        MaxTemperature = 28,
                        WeatherType = WeatherTypes.Sunny
                    },
                    new WeatherInfo{
                        Id = 2,
                        City = "Minsk",
                        Date = DateTime.Today.AddDays(1),
                        MinTemerature = 23,
                        MaxTemperature = 26,
                        WeatherType = WeatherTypes.PartlyCloudy
                    },
                };

        // GET api/weatherInfo
        [HttpGet]
        public IEnumerable<WeatherInfo> Get()
        {
            return weatherInfoList;
        }

        // GET api/weatherInfo/2
        [HttpGet("{id}")]
        public WeatherInfo Get(int id)
        {
            return weatherInfoList.FirstOrDefault(wi => wi.Id == id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        // DELETE api/values/5
        [HttpPatch("{id}")]
        public void Patch(int id, [FromBody]string value)
        {
        }
    }
}
