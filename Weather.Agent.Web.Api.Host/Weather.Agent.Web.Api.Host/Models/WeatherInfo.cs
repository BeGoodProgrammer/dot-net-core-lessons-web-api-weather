﻿using System;

namespace Weather.Agent.Web.Api.Host.Models
{
    public class WeatherInfo
    {
        public int Id { get; set; }

        public int MinTemerature { get; set; }

        public int MaxTemperature { get; set; }

        public string City { get; set; }
        
        public DateTime Date { get; set; }

        public WeatherTypes WeatherType { get; set; }
    }
}
